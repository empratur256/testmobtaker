<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::whereEmail($request->email)->first();
        $token = null;
        if (Hash::check($request->password ,$user->password) ) {
            $token = $user->createToken(config('app.name'))->accessToken;
            return ['access_token' => $token];
        }
        return response()->json([trans('auth.message')], 422);
    }
}
