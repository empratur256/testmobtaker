<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TeacherAddRequest;
use App\Http\Requests\Admin\TeacherEditRequest;
use App\Http\Resources\TeacherResource;
use App\Http\Resources\UserResource;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
          return TeacherResource::collection(Teacher::paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return TeacherResource
     */
    public function store(TeacherAddRequest $request)
    {
        return new TeacherResource(Teacher::create($request->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Teacher $teacher
     * @return TeacherResource
     */
    public function show(Teacher $teacher)
    {
        return new TeacherResource($teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Teacher $teacher
     * @return TeacherResource
     */
    public function update(TeacherEditRequest $request, Teacher $teacher)
    {
        $teacher->update($request->toArray());
        return new TeacherResource($teacher);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Teacher $teacher
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Teacher $teacher)
    {
        $teacher->delete();
        return response()->json([trans('teacher.delete')]);
    }
}
