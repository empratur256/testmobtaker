<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonsAddRequest;
use App\Http\Requests\Admin\LessonsEditRequest;
use App\Http\Resources\LessonResource;
use App\Http\Resources\TeacherResource;
use App\Http\Resources\UserResource;
use App\Models\Lesson;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return LessonResource::collection(Lesson::paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return LessonResource
     */
    public function store(LessonsAddRequest $request)
    {
        $data = Lesson::create($request->except(['teacher_ids']));
        $data->teacher()->toggle($request->teacher_ids);
        return new LessonResource(Lesson::create($request->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param Lesson $lesson
     * @return LessonResource
     */
    public function show(Lesson $lesson)
    {
        return new LessonResource($lesson);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Lesson $lesson
     * @return LessonResource
     */
    public function update(LessonsEditRequest $request, Lesson $lesson)
    {
        $lesson->update($request->except(['teacher_ids']));
        $lesson->teacher()->toggle($request->teacher_ids);
        return new LessonResource($lesson);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Lesson $lesson
     * @return Response
     * @throws Exception
     */
    public function destroy(Lesson $lesson)
    {
        $lesson->delete();
        return response()->json([trans('lesson.delete')]);
    }
}
