<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StudentAddRequest;
use App\Http\Requests\Admin\StudentEditRequest;
use App\Http\Resources\StudentResource;
use App\Http\Resources\UserResource;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return StudentResource::collection(Student::paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return StudentResource
     */
    public function store(StudentAddRequest $request)
    {
        return new StudentResource(Student::create($request->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return StudentResource
     */
    public function show(Student $student)
    {
        return new StudentResource($student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Student $student
     * @return StudentResource
     */
    public function update(StudentEditRequest $request, Student $student)
    {
        $student->update($request->toArray());
        return new StudentResource($student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return response()->json([trans('student.delete')]);
    }
}
