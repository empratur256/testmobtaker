<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonsAddRequest;
use App\Http\Requests\LessonsEditRequest;
use App\Http\Resources\LessonResource;
use App\Http\Resources\TeacherResource;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LessonResource::collection(Auth::user()->lesson()->paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonsAddRequest $request)
    {
        $data = Auth::user()->lesson()->create($request->except(['teacher_ids']));
        $data->teacher()->toggle($request->teacher_ids);
        return new LessonResource(Auth::user()->lesson()->create($request->except(['teacher_ids'])));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        return new LessonResource(Auth::user()->lesson()->find($lesson->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(LessonsEditRequest $request, Lesson $lesson)
    {
        Auth::user()->lesson()->find($lesson->id)->update($request->except(['teacher_ids']));
        $lesson->teacher()->toggle($request->teacher_ids);
        return new LessonResource($lesson);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        Auth::user()->lesson()->find($lesson->id)->delete();
        return response()->json([trans('teacher.delete')]);
    }
}
