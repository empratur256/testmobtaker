<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function lesson()
    {
        return $this->belongsToMany(Lesson::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
