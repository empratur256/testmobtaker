<?php

use App\Http\Controllers\Api\Admin\LessonController;
use App\Http\Controllers\Api\Admin\StudentController;
use App\Http\Controllers\Api\Admin\TeacherController;
use App\Http\Controllers\Api\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('login', [LoginController::class, 'login']);

Route::middleware(['auth:api'])->group(function () {
    Route::middleware(['super_admin'])->prefix('admin')->name('admin.')->group(function () {
        Route::resource('users', UserController::class)->except(['edit', 'create']);
        Route::resource('teachers', TeacherController::class)->except(['edit', 'create']);
        Route::resource('students', StudentController::class)->except(['edit', 'create']);
        Route::resource('lessons', LessonController::class)->except(['edit', 'create']);
    });
    Route::resource('teachers', TeacherController::class)->except(['edit', 'create']);
    Route::resource('students', StudentController::class)->except(['edit', 'create']);
    Route::resource('lessons', LessonController::class)->except(['edit', 'create']);
});
